
# Ecliptica

## Ecliptica is website template aiming to follow the best standards, using the newest technology available.

The technology planed to use:
 * VSC (GIT)
 * Type-safe language
 * UI Framework
 * SSR
 * CSS-in-JS library
 * Linter
 * Formatter
 * Headless CMS

Desirable features:
 * Theming support
 * Multi-language support

Misc goals regarding project management:
 * GIT:
	* Well written and formatted commit messages
	* Systematic naming and working with branches
	* Usage of tags for releases
	* No commits other than merge commits on `master`
 * Issue management (on GitLab)
 * Issue-driven development
